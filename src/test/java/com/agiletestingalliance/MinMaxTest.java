package com.agiletestingalliance;

import org.junit.Assert;
import org.junit.Test;

public class MinMaxTest{

    @Test
    public void testGetMax() {
        Assert.assertTrue(new MinMax().getMax(1,2)==2);
    }
}