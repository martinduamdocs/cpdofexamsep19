package com.agiletestingalliance;

public class MinMax {

    public int getMax(int first, int second) {
        if (second > first) {
            return second;
        }else {
            return first;
        }
    }

}
